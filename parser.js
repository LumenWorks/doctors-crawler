Date.prototype.toFormat = function() {
   var yyyy = this.getFullYear().toString();
   var mm = (this.getMonth()+1).toString();
   var dd  = this.getDate().toString();
   return Array((dd[1]?dd:"0"+dd[0]), (mm[1]?mm:"0"+mm[0]), yyyy ).join('.');
};

var config = {
        login: 'vk.arabesque@yandex.ru',
        password: '81kD2d',
        target: 'http://hh.ru/search/resume?exp_period=all_time&exp_period=all_time&order_by=publication_time&area=1&area=2019&text=%D0%9F%D0%B5%D0%B4%D0%B8%D0%B0%D1%82%D1%80&text=%D0%9C%D0%B5%D0%BD%D0%B5%D0%B4%D0%B6%D0%B5%D1%80%2C+%D0%BC%D0%B5%D0%B4%D0%B8%D1%86%D0%B8%D0%BD%D1%81%D0%BA%D0%B8%D0%B9+%D0%BF%D1%80%D0%B5%D0%B4%D1%81%D1%82%D0%B0%D0%B2%D0%B8%D1%82%D0%B5%D0%BB%D1%8C&pos=position&pos=position&logic=normal&logic=except&mark=main_page_last_search_6',
        start: 25,
        end: 50,
        category: 'Педиатры'
    },
    parser = {
        getPages: function() {
            var pages = [];
            for (var i = config.start; i < config.end; i++) {
                pages.push(config.target + '&page=' + i);
            }
            return pages;
        },
        getLinks: function() {
            var links = document.querySelectorAll("a.output__name");
            var array = [];
            for (var i = 0; i < links.length; i++) {
                array[i] = links[i].href;
            }
            return array;
        },
        getLocation: function(href){
            var link = document.createElement("a");
            link.href = href;
            return link;
        }
    },
    arrayProcessResume = [], 
    arrayResultResume = [],
    fs = require('fs'),
    casper = require('casper').create({
        waitTimeout: 15000,
        stepTimeout: 15000,
        verbose: true,
        logLevel: "warning",
        viewportSize: {
            width: 1280,
            height: 800
        },
        pageSettings: {
            "userAgent": 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
            "webSecurityEnabled": false,
            "ignoreSslErrors": true
        },
        onWaitTimeout: function() {
            console.log('Wait TimeOut Occured');
        },
        onStepTimeout: function() {
            console.log('Step TimeOut Occured');
        }
    });

// OK GO

casper.start('http://hh.ru/login', function() {
    console.log('Попытка соединения...');
    this.fill('form', {
        'username': config.login,
        'password': config.password
    }, true);
});

casper.then(function() {
    if(this.exists("div.account-message_error")) {
        console.log(this.fetchText('div.account-message_error'));
        return setTimeout(function(){ casper.exit(); }, 0);
    } else {
        console.log("Авторизован как " + this.fetchText('span.navi-item__post'));
        console.log('Собираю ссылки со страниц целевого запроса...');
        this.wait(2000);
    }
});

casper.each(parser.getPages(), function(self, url) {
    casper.thenOpen(url, function() {
        var links = this.evaluate(parser.getLinks);
        for (var i = 0; i < links.length; i++) {
            arrayProcessResume.push(links[i]);
        }
    });
});

casper.then(function() {

    console.log('Грабблю резюме, всего:', arrayProcessResume.length);
    var completeCount = 1;
    this.each(arrayProcessResume, function(self, url) {
        

        casper.thenOpen(url, function() {
            console.log('Граббинг (', completeCount++, '/', arrayProcessResume.length, ')');

            var result = this.evaluate(function(){
                var education = $('[itemtype="http://schema.org/EducationalOrganization"] [itemprop=name]')[0],
                    workPlace = $('[itemtype="http://schema.org/Organization"] .resume__experience__company')[0],
                    workPosition = $('[itemtype="http://schema.org/Organization"] .resume__experience__position')[0];

                return {
                    email: $('[itemprop="email"]').text(),
                    name: $('.resume__personal__name').text(),
                    profession: document.title,
                    birthday: $('[itemprop=birthDate]').attr('content'),
                    phone: $('.resume__contacts__phone__number').text().trim().split('\n')[0],
                    from: $('.resume__inlinelist__item [itemprop=addressLocality]').text(),
                    education: education.textContent,
                    workPlace: workPlace.textContent,
                    workPosition: workPosition.textContent
                }
            });
            if (!result) 
                console.log('Не получен результат парсинга');
            else {
                result.toJSON = function () {
                    var copy = {};
                    copy.email = result.email;
                    copy.name = result.name;
                    copy.profession = result.profession;
                    copy.birthday = result.birthday;
                    copy.phone = result.phone;
                    copy.from = result.from;
                    copy.education = result.education;
                    copy.workPlace = result.workPlace;
                    copy.workPosition = result.workPosition;
                    copy.category = config.category;
                    return copy;
                };
                arrayResultResume.push(result);
                console.log('Профиль добавлен:', ((result.name == '  ') ? 'Данные недоступны' : result.name));
            }
        });
    });
});

casper.then(function() {
    var targetFile = fs.pathJoin(fs.workingDirectory, 'files', config.category + "_" + new Date().toFormat());
    var json = JSON.stringify(arrayResultResume);
    fs.write(targetFile + ".json", json, 'w');
});


casper.thenOpen('http://hh.ru/logoff.do', function() {
    console.log("Завершение сессии");
});


casper.run(function() {
    this.exit();
});
